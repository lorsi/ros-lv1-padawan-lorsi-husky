# Simple "Get out of the Maze" Husky Robot Controller

## Disclaimer
This project is intended for learning purposes.

## Overview
The main goal of this project is to put into practice ROS basics. It consists of a simulation of a Husky robot using a simple `wall follow` algorithm to get out of a Maze. Husky must be able to get out of what is known as a __simply connected maze__ without any prior knowledge of its structure. 

## How to run it

### Prequisites
- ROS melodic.
- Gazebo 9 

### Instructions
Open a terminal and run the following commands

```bash
cd <your_catkin_ws/src>
git clone https://gitlab.com/lorsi/ros-lv1-padawan-husky <destination folder>
cd .. && rosdep install --from-path src -yi
cd catkin_make 
source devel/setup.bash
roslaunch lorsi_padawan_lv1_husky_maze husky_maze.launch
```

This will open up a Gazebo window and the simulation will start running.

## Project architecture
To begin with, it's convenient to have a look at what `husky_maze.launch` does.
This launchfile sets up and runs the following:
- `husky_empty_world`:
    - Using custom maze world, located in `worlds/complex_maze.world`
    - Enabling Husky's Laser sensors.
- `husky_maze_node`
    - The custom node coded in this project. In charge of getting husky out of the maze.

## About "Husky Maze Node"
The node coded in this project communicates as follows:
- Input:
    - Receives Laser information from Husky by subscribing to `/scan`
- Output:
    - Publish velocity commands to `/cmd_vel`

- To begin with, the Node transforms incoming laser information into a simple data structure, `Clearance`. 
- Such structure simply contains information about whether North, East and West are clear (from Husky's perspective).
- Having the `Clearance` updated periodically, `HuskyDecider` is then in charge of deciding where to move in terms of clearance data.
- Finally, `HuskyController` sends velocity commands to navigate husky in terms of the decision taken by `HuskyDecider`.

This is how the node communication graph looks like when running the whole stack.
![RQTGraph](images/RQTGraph.png)


### Clearance estimation
The following two approaches were attempted.

#### Beam Grouping
The first approach was to group Laser beams as follows (considering 0deg as the front of the robot):

    - West: Every beam from the leftmost limit of the lidar up to -45deg.
    - North: Every beam from -45deg to 45deg.
    - East: Every beam from 45deg to the rightmost beam of the lidar.

Once lasers were grouped and a distance threshold value was chosen, if a specific percentaje of the beams of one group reported a distance lower than the threshold, its zone would be labeled as `blocked`. Otherwise, it would be `clear`.

#### Beam Selection
This approach, which ended being the one used in the final implementation, just chooses 1 beam for each relevant direction (East, North, West). This is no good practice in real life as depending on a single beam is error prone given that sensors are not ideal. However, in this simulation, beams' values were consistent enough to make the whole node work with this approach.
The advantage of this rule, in contrast to the previously described `Beam Grouping`, is that directivity is a lot better, allowing the clearance estimation to be more accurate.
Something relevant to note is that, since the robot ended up being a "left-wall follower" shifting the North estimation a bit to the left helped in preventing the robot from detecting false north clearances when turning. 

### Husky Controller

### Using `move_base`
Probably, this approach would've returned the best results. However, this required prior mapping of the maze which would've ruined the whole point of the project (which is leaving the robot to its own luck in an unknown maze). 

### Using Odometry Feedback and Velocity Commands
The idea behind using odometry was the following:
- Making the robot move X meters forwards.
- Run `HuskyDecider` to know where to go in terms of the clearance.
- Move the robot. If turning was needed, first it would turn the robot in place and then move forwards X meters.

This solution was discarded as it made the logic way more complex and didn't add any aditional help to make a simple wall following algorithm. 

## Using Velocity Commands
The simplest, but yet the chosen approach for this project. It basically either moves the robot forward, to the left or to the right depending on `HuskyDecider`'s output. 
It's relevant to note that, in contrast to the odometry solution mentioned above, turning was designed to happen with both angular and linear velocity. This was hard to adjust in the beginning, but made the robot's motion a lot smoother. 

## I'm lazy to make this run in my PC, show me how it looks like!
I fully understand if that's your case, as getting ros environments up is kinda painful if you have never used ros in you computer, so here is a small laggy gif of Husky going through the maze.

![HuskyGif](images/HuskySolvingMaze.gif)




