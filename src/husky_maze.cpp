#include <ros/ros.h>
#include <math.h>
#include <std_msgs/String.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <algorithm>
#include <vector>
#include <sstream>

#include "husky_controller.cpp"
#include "husky_decider.cpp"
#include "motion_request.cpp"
#include "clearance.cpp"

/** Constants to be loaded from parameter server. */
double ANGLE_MIN_RAD;
double ANGLE_MAX_RAD;
double ANGLE_STEP_RAD;

double N_RAD;
double W_RAD;
double E_RAD;

double CLEARANCE_DISTANCE;
double FORWARD_SPEED;
double LEFT_LINEAR_SPEED;
double RIGHT_LINEAR_SPEED;
double LEFT_ANG_SPEED;
double RIGHT_ANG_SPEED;

/** State Variables. */
Clearance g_clr; // Continously updated. Shows the clearance (W,N,E) of the robot's surroundings.
std::shared_ptr<ClearanceEstimator> estimator;


/** Called when a Laser scan is received. Converts it to clearance and updates \link g_clr \endlink */
void onScanReceived(const sensor_msgs::LaserScan::ConstPtr& msg) {
  g_clr = estimator->estimate(msg->ranges);
}

/**
 * This tutorial demonstrates simple sending of messages over the ROS system.
 */
int main(int argc, char **argv)
{
  /* Ros Node setup and params loading.*/
  ros::init(argc, argv, "husky_maze");
  ros::NodeHandle n;
  n.getParam("/angle_min_rad", ANGLE_MIN_RAD);
  n.getParam("/angle_max_rad", ANGLE_MAX_RAD);
  n.getParam("/angle_step_rad", ANGLE_STEP_RAD);

  n.getParam("/n_rad", N_RAD);
  n.getParam("/w_rad", W_RAD);
  n.getParam("/e_rad", E_RAD);

  n.getParam("/clearance_distance", CLEARANCE_DISTANCE);
  n.getParam("/forward_speed", FORWARD_SPEED);
  n.getParam("/left_linear_speed", LEFT_LINEAR_SPEED);
  n.getParam("/right_linear_speed", RIGHT_LINEAR_SPEED);
  n.getParam("/left_ang_speed", LEFT_ANG_SPEED);
  n.getParam("/right_ang_speed", RIGHT_ANG_SPEED);

  /* Reorganizing parameters. */
  const LaserSpecs laserSpecs = {
    ANGLE_MIN_RAD,
    ANGLE_MAX_RAD,
    ANGLE_STEP_RAD
  };

  const ClearanceThreshold clearanceThreshold = {
    N_RAD, 
    W_RAD,
    E_RAD
  };

  const ControlParams control_params = {
    FORWARD_SPEED,
    LEFT_LINEAR_SPEED,
    RIGHT_LINEAR_SPEED,
    LEFT_ANG_SPEED,
    RIGHT_ANG_SPEED
  };

  estimator = std::make_shared<ClearanceEstimator>(clearanceThreshold, laserSpecs, CLEARANCE_DISTANCE);

  /* Subscribes to `odometry` and `laser`. Publishes `twist` messages to husky. */
  auto scan_sub = n.subscribe("scan/", 5, onScanReceived);
  ros::Publisher vel_pub = n.advertise<geometry_msgs::Twist>("cmd_vel", 100);
  ros::Rate r(100);

  /* Control and Decision taking classes. */
  std::shared_ptr<HuskyDecider> decider = std::make_shared<HuskyDecider>(g_clr);
  std::shared_ptr<HuskyController> controller = std::make_shared<HuskyController>(vel_pub, control_params);

  /* Main loop. */
  while (ros::ok()) {
    MotionRequest motion_request = decider->decide();
    controller->move(motion_request);
    ros::spinOnce();
    r.sleep();
  }

  return 0;
}
