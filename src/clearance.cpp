#pragma once

struct LaserSpecs {
    double angle_min_rad;
    double angle_max_rad;
    double angle_step_rad;
};

struct ClearanceThreshold {
    double n_rad;
    double w_rad;
    double e_rad;
};


/* Gets the closest array index to match a specific angle from a range of measurements. */
uint32_t angleToIndex(
  double angle, 
  const LaserSpecs& specs) {
  angle = std::min(std::max(angle, specs.angle_min_rad), specs.angle_max_rad);
  return static_cast<uint32_t>((angle - specs.angle_min_rad) / specs.angle_step_rad);
}

/** Shows whether the path is clear or not in the 3 main directions (left, up, right). */
class Clearance {
 public: 
  Clearance(bool n=false, bool e=false, bool w=false) {
      clearance =  (uint8_t)w | ((uint8_t)n) << 1 | ((uint8_t)e) << 2; 
  }
  
  void operator=(const Clearance& other) {
   clearance = other.clearance;
  }

  bool forwardClear() const {
    return clearance & 0b010;
  }

  bool westClear() const {
    return clearance & 0b001;
  }

  bool eastClear() const {
    return clearance & 0b100;
  }

private:
  uint8_t clearance = 0b000;
};

class ClearanceEstimator {
 public:
    ClearanceEstimator(
        const ClearanceThreshold& clearance_thresh, 
        const LaserSpecs& laser_specs, 
        const double& distance_threshold_m): 
            clearance_thresh(clearance_thresh), 
            laser_specs(laser_specs), 
            distance_threshold_m(distance_threshold_m) {
                north_index = angleToIndex(clearance_thresh.n_rad, laser_specs);
                east_index = angleToIndex(clearance_thresh.e_rad, laser_specs);
                west_index = angleToIndex(clearance_thresh.w_rad, laser_specs);
            }

    Clearance estimate (const std::vector<float>& ranges) const {
        return Clearance(
            ranges[north_index] > distance_threshold_m,
            ranges[east_index] > distance_threshold_m,
            ranges[west_index] > distance_threshold_m
        );
    }

 private:
    const ClearanceThreshold& clearance_thresh;
    const LaserSpecs& laser_specs;
    const double distance_threshold_m;

    uint32_t north_index;
    uint32_t east_index;
    uint32_t west_index;
};