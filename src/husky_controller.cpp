#pragma once

#include <ros/ros.h>
#include <math.h>
#include <std_msgs/String.h>
#include <nav_msgs/Odometry.h>
#include <geometry_msgs/Twist.h>
#include <sensor_msgs/LaserScan.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <algorithm>
#include <vector>
#include <sstream>

#include "motion_request.cpp"

struct ControlParams {
    double forward_speed;
    double left_linear_speed;
    double right_linear_speed;
    double left_ang_speed;
    double right_ang_speed;
};

/** Converts linear/angular speed to Twist messages */
geometry_msgs::Twist twistBuilder2D(double x_m, double y_m, double rad) {
  geometry_msgs::Twist twist;
  twist.linear.x = x_m;
  twist.linear.y = y_m;
  twist.angular.z = rad;
  return twist;
} 

/** In charge of the Husky's control, this class knows how to make the robot move forward and turn. 
 * 
 * Currently it works without any odometry feedback. 
 * 
*/
class HuskyController {
  public:

    /** Creates a HuskyController.
     * 
     * @param pub requires the Husky's cmd_vel publisher to perform the control. 
     * 
    */
    HuskyController(const ros::Publisher& pub, const ControlParams& control_params ) :
        _cmd_vel_publisher(pub),  
        _control_params(control_params)
        {}

    void move(MotionRequest direction) {
        switch (direction) {
            case MotionRequest::FORWARD:
                _move(_control_params.forward_speed, 0.0);
                break;
            case MotionRequest::LEFT:
                _move(_control_params.left_linear_speed, _control_params.left_ang_speed);
                break;
            case MotionRequest::RIGHT:
                _move(_control_params.right_linear_speed, _control_params.right_ang_speed);
                break;
        }
    }

  private:
    void _move(double linear_speed, double angular_speed) {
        _cmd_vel_publisher.publish(twistBuilder2D(linear_speed, 0.0, angular_speed));
    }

    const ros::Publisher& _cmd_vel_publisher;
    const ControlParams& _control_params;
};