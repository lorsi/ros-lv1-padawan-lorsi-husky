#pragma once

/** Output of a HuskyDecider. A request to be made to the controller. Just the basic movements can be requested. */
enum class MotionRequest {LEFT, RIGHT, FORWARD, NONE};