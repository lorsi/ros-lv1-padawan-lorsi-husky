#pragma once

#include <algorithm>
#include <math.h>
#include <ros/ros.h>
#include <sstream>
#include <vector>

#include "clearance.cpp"
#include "motion_request.cpp"

/** Based on the current clearance, the HuskyDecider is in charge of deciding where the robot should move next.*/
class HuskyDecider {
  public:

    /** Creates a decision tree.
     * 
     * @param observable_clearance a @code Clearance object that gets updated periodically.
     * 
     */
    HuskyDecider(const Clearance& observable_clearance) : _clearance(observable_clearance) {}

    /** Decides, based on the current clearance, where the robot should move to. 
     * 
     * @return the move request to be passed on to the controller.
     * 
    */
    MotionRequest decide() {
        if (_clearance.forwardClear()) { // If path is clear moving forwards...
            ROS_INFO("[Decider] Forward is clear.");
            if (_clearance.westClear()) { // ... but also left...
                ROS_INFO("[Decider] Should move left to wall follow.");
                return MotionRequest::LEFT; // ... it moves left.
            } else { // If left is blocked and forward clear...
                ROS_INFO("[Decider] Following wall, move forward.");
                return MotionRequest::FORWARD; // ... it moves forward.
            }
        } else { // If forward is not clear...
            ROS_INFO("[Decider] Forward is NOT clear.");
            if (_clearance.westClear()) { // ... and can move left ...
                ROS_INFO("[Decider] We should move left.");
                return MotionRequest::LEFT; // ... moves left.
            } else { // If left and forward are blocked...
                ROS_INFO("[Decider] Only right is an option.");
                return MotionRequest::RIGHT; // ... only then it moves right.
            }
        }
        ROS_INFO("[Decider] Situation not covered.");
        return MotionRequest::NONE;
    }

  private:
    const Clearance& _clearance;

};
